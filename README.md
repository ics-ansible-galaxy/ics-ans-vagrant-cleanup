# ics-ans-vagrant-cleanup

Ansible playbook to run the VirtualBox VM cleanup script found in the Vagrant role. 
Script can be found in this path: /usr/local/bin/clean-virtualbox and be modified in ics-ans-role-vagrant/templates

The vagrant box cleanup is triggered once a week through AWX with the variable defined in the job template

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## License

BSD 2-clause
